#!/bin/bash

rm -rf public
mkdir -p public

echo "Generating docs for:"
git for-each-ref --format='%(refname:lstrip=3)' refs/remotes/origin/master refs/remotes/origin/v*

echo "<em>Read the docs:</em>" >> public/index.html
echo "<ul>" >> public/index.html

IFS=$'\n';for ref in $(git for-each-ref --format='%(refname:lstrip=3)' refs/remotes/origin/master refs/remotes/origin/v*); do
  git checkout $ref
  mkdir -p public/$ref
  cp -r src/* public/$ref/
  echo "<li><a href='/$ref/index.html'>$ref</a></li>" >> public/index.html
done

echo "</ul>" >> public/index.html
